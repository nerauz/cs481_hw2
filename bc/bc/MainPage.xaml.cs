﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace bc
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void clear(object sender, EventArgs e)
        {
            this.input.Text = "0";
        }

        private bool isOperator(char value)
        {
            return value == '+' || value == '-' || value == '*' || value == '/' ||value == '.';
        }

        public void add(object sender, EventArgs e)
        {
            string btnValue = (sender as Button).Text;
            char[] inputAsChar = input.Text.ToCharArray();

            if (isOperator(btnValue[0]))
            {
                char lastChar = inputAsChar[inputAsChar.Length - 1];

                if (isOperator(lastChar))
                {
                    inputAsChar[inputAsChar.Length - 1] = btnValue[0];
                    input.Text = new string(inputAsChar);
                    return;
                }
            }
            
            string newInput = (new string(inputAsChar) + btnValue).TrimStart('0');

            input.Text = newInput.Length <= 0 ? "0" : newInput;
        }

        public void calculate(object sender, EventArgs e)
        {
            if (isOperator(this.input.Text[this.input.Text.Length - 1]))
                return;
            
            string input = this.input.Text;
            double result = Convert.ToDouble(new DataTable().Compute(input, null));
            
            this.input.Text = result.ToString();
        }

    }
}
